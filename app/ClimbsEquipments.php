<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClimbsEquipments extends Model
{
    protected $table = 'g_rlt_climbs_equipments';

    protected $fillable = ['id_climb', 'id_equipment', 'amount'];

    public $timestamps = false;


        public function equipments()
        {
            return $this->belongsTo('App\Equipments', 'id_equipment', 'id');
        }

        public function climbs()
        {
            return $this->belongsTo('App\Climbs', 'id_climb', 'id');
        }
}
