@extends('layouts.app', [
            'title'         => 'Climb',
        ])


@section('content')
<div class="content">
<div class="container-fluid">

    @include('pages.climbs.includes.info')

    <div class="row mb-4">
        @include('pages.climbs.includes.description')
    </div>

    <div class="row mb-4">
        @include('pages.climbs.includes.details')

        @include('pages.climbs.includes.weather_obs')
    </div>

    <div class="row mb-4">

    @include('pages.climbs.includes.equipments')

    @include('pages.climbs.includes.firstascent')

    </div>

</div>
</div>
@endsection


@push('js')
<script src="{{ asset('js') }}/typeahead.min.js"></script>

@stack('js-include')

<script>

showEquipments({!! json_encode($climb_equipments) !!});
showFirstAscent({!! json_encode($climb_firstascent) !!});

</script>

@endpush
