<div class="col-xl-4">

<div id="card_details" class="card">

<div class="card-header py-3">
<div class="row">
    <div class="col">
        {{ __('Detalhes') }}
    </div>

    <div class="col text-right">
        <button id="btn_form_details" class="btn btn-sm btn-primary rounded-circle text-white p-1 px-2 ml-3"
            data-toggle="tooltip" data-placement="right" title="{{ __('Editar') }}">
            <i class="ni ni-ruler-pencil"></i>
        </button>
    </div>
</div>
</div>

<div class="card-body pt-3">
Extensão<br>
Cordadas<br>
Data Conquista<br>
</div>

</div>


</div>