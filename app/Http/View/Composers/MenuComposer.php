<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;

use App\Crags;

class MenuComposer
{

    protected $crags;


    public function __construct()
    {
        $this->crags = Crags::orderby('name')->get();
    }


    public function compose(View $view)
    {
        $view->with('MenuLocations', $this->crags);
    }
}
