@extends('layouts.app', [
            'title'         => 'Climbs',
        ])


@section('content')
    <div class="content">
    <div class="container-fluid">

        <div class="row">
        <div class="col-md-12">

<!-- FORM -->
            <div class="col-8 offset-2 bg-form p-4 rounded">

                {{ Form::open(array('route' => 'climbs.store', 'method' => 'post')) }}

                {{ Form::text('id_location', $id, array('class' => 'form-control')) }}

                <div class="form-row mb-3">
                    {{ Form::label('title', 'Nome') }}
                    {{ Form::text('title', null, array('class' => 'form-control')) }}
                </div>

                    {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}
            </div>
<!-- END: FORM -->

</div>
</div>

    </div>
    </div>
@endsection
