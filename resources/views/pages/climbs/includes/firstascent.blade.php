    <div class="col-xl-8">

<!-- BEGIN: Card Conquistadores -->
        <div id="card_firstascent" class="card">

            <div class="card-header border-0">
                <div class="row align-items-center">
                <div class="col">
                    <h3 class="mb-0">{{ __('Conquistadores') }}</h3>
                </div>

                <div class="col text-right">
                    <button id="btn_form_firstascent" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="right" title="{{ __('Adicionar') }}">
                        <i class="ni ni-fat-add"></i>
                    </button>
                </div>
                </div>
            </div>

            <div class="table-responsive">
                <table id="table_firstascent" class="table align-items-center table-flush">

                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Nome</th>
                        <th scope="col">&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    </tbody>

                </table>
            </div>
        </div>
<!-- END: Card Conquistadores -->


<!-- BEGIN: Form Conquistadores -->
        <div id="card_form_firstascent" class="card d-none">
            <div class="card-header">
                <h3 class="mb-0">{{ __('Adicionar Conquistador') }}</h3>
            </div>

            <div class="card-body">

                {{ Form::open(array('id' => 'form_add_firstascent', 'route' => 'climbs.firstascent.store.ajax', 'method' => 'post')) }}

                {{ Form::hidden('id_climb', $climb->id, array('id' => 'id_climb', 'class' => 'form-control')) }}

                    <div class="form-row mb-3">
                        <div class="col">
                            {{ Form::label('firstascent', __('Conquistador'), array('class' => 'small font-weight-bold')) }}
                            {{ Form::text('firstascent', null, array('id' => 'firstascent', 'class' => 'form-control', 'placeholder' => 'Selecione')) }}
                        </div>
                    </div>

                    {{ Form::submit(__('Salvar'), array('id' => 'btn_save_firstascent', 'class' => 'btn btn-small btn-primary')) }}
                    {{ Form::button(__('Cancelar'), array('id' => 'btn_cancel_form_firstascent', 'class' => 'btn btn-small btn-danger')) }}

                {{ Form::close() }}
            </div>
        </div>
<!-- END: Form Conquistadores -->

    </div>


@push('js-include')
<script>
/* ----- ----- ----- ----- ----- ----- ----- -----
   --- JS: FirstAscent
   ----- ----- ----- ----- ----- ----- ----- -----  */

// --- BTN: Controles para Data/Form dos CONQUISTADORES
 $('#btn_form_firstascent, #btn_cancel_form_firstascent').on('click',function(){
  resetFormFirstAscent();
  $('#card_firstascent, #card_form_firstascent').toggleClass('d-none');
 });

// --- INPUT: autocomplete para campo de Conquistadores
    $('#form_add_firstascent #firstascent').typeahead({
        source: function (query, process) {
            return $.get("{{ route('climbs.firstascent.autocomplete.ajax') }}", { query: query }, function (data) {
                return process(data);
            });
        }
    });

// --- FORM: criar registro de conquistador
    $( "#form_add_firstascent" ).submit(function( event ) {
    event.preventDefault();

    var id_climb = $(this['id_climb']).val();
    var name = $(this['firstascent']).val();

        $.ajax({
            type        : 'POST',
            url         : '{{ route("climbs.firstascent.store.ajax") }}',
            dataType    : "JSON",
                data    : {
                    "_token": "{{ csrf_token() }}",
                    id_climb: id_climb,
                    name: name,
                },
                    success: function(data){
                        var alert = '<strong>Info!</strong> Registro Ok!';

                            showFirstAscent(data);
                            callAlerts(alert, 'primary');

                            resetFormFirstAscent();
                            $('#card_firstascent, #card_form_firstascent').toggleClass('d-none');
                    }
        });
    return false;
    });

// --- BTN-ACTION: excluir registro de conquistador
    $('#table_firstascent tbody').on('click','.btn-danger',function(){

    var id = $(this).data('id').split('_');

        $.ajax({
            type        : 'POST',
            url         : '{{ route("climbs.firstascent.destroy.ajax") }}',
            dataType    : "JSON",
            data    : {
                    "_token": "{{ csrf_token() }}",
                    id_climb: id[0],
                    id_firstascent: id[1],
                },
                    success: function(data){
                        var alert = '<strong>Info:</strong> registro removido!';
                        showFirstAscent(data);
                        callAlerts(alert, 'primary');
                            resetFormFirstAscent();
                    }
        });
    return false;
    });

// --- Reset dos campos do formulário de conquistadores
        function resetFormFirstAscent() {
            $('#form_add_firstascent input[name="firstascent"]').val("");
        }

// --- Alimenta layout de conquistadores
        function showFirstAscent(data) {
        var html = '';
        var i;

            for(i=0; i<data.length; i++)
            {
            html += '<tr>' +
                        '<th scope="row">' + data[i].name + '</th>' +
                        '<td class="text-right">' +
                        '<button class="btn btn-sm btn-danger align-top rounded-circle text-white p-1 px-2 ml-3" ' +
                            'data-id="' + data[i].id_climb + '_' + data[i].id_firstascent + '"' +
                            'data-toggle="tooltip" data-placement="right" title="{{ __('Remover') }}">' +
                            '<i class="ni ni-fat-remove"></i>' +
                        '</button>' +
                        '</td>' +
                    '</tr>';
            }

            $('#table_firstascent tbody').html(html);
        }
</script>
@endpush