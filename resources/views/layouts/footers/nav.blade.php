<div class="row align-items-center justify-content-xl-between">
    <div class="col-12">
        <div class="copyright text-right text-muted small">
            &copy; {{ now()->year }}
            -
            <a href="https://www.creative-tim.com/product/argon-dashboard" class="font-weight-bold ml-1" target="_blank">Argon Dashboard</a>
            --
            <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a> &amp;
            <a href="https://www.updivision.com" class="font-weight-bold ml-1" target="_blank">Updivision</a>
            &nbsp;&nbsp;
            (<a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="small text-secondary" target="_blank">MIT License</a>)
        </div>
    </div>
</div>
