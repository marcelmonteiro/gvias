<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Crags;
use App\Climbs;

use App\Equipments;
use App\Styles;
use App\Exposure;
use App\Grades;
use App\FirstAscent;

use App\StylesGrades;
use App\ClimbsEquipments;
use App\ClimbsFirstAscent;


class ClimbsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function show($id)
    {
        $climb = Climbs::where('id', $id)->first();

        $ndx_styles = Styles::orderby('title')->get();
        $ndx_exposure = Exposure::orderby('title')->get();

            $idStyle = $climb->id_style;
        $ndx_grades = Grades::whereHas('stylesGrades', function($q) use($idStyle) {
            $q->where('id_style', $idStyle);
        })->get();

        $ndx_equipments = Equipments::orderby('title')->get();

        $climb_firstascent = $this->getClimbFirstAscent($id);
        $climb_equipments = $this->getClimbEquipments($id);


/*            $climb_equipments = array();
            if(isset($climb->equipments) && !$climb->equipments->isEmpty()) {
                foreach($climb->equipments as $t) {
                    $climb_equipments[] = [ 'id'            => $t->id,
                                            'title'         => $t->title,
                                            'id_climb'      => $t->pivot->id_climb,
                                            'id_equipment'  => $t->pivot->id_equipment,
                                            'amount'        => $t->pivot->amount,]; }
            }                           */

        return view('pages.climbs.climb', compact(
                                            'climb',
                                            'ndx_equipments',
                                            'ndx_styles',
                                            'ndx_exposure',
                                            'ndx_grades'))
                    ->with(['climb_equipments' => $climb_equipments,
                            'climb_firstascent' => $climb_firstascent]);
    }


    public function create($id)
    {
        return view('pages.climbs.create', compact('crags', 'id'));
    }


    public function store(Request $request)
    {
            $action = Climbs::firstOrCreate(
                            $request->only('name', 'id_crag'),
                            [$request->only('id_style', 'id_grade', 'id_exposure'), 'id_user' => Auth::id()]
                        );

        return redirect()->route('home');
    }


    function update(Request $request)
    {
        $action = Climbs::findOrFail($request->id);

            $action->name           = $request->name;
            $action->id_style       = $request->id_style;
            $action->id_grade       = $request->id_grade;
            $action->id_exposure    = $request->id_exposure;

        $action->save();

            if($action->wasChanged()) {
                $climb = Climbs::where('id', $action->id)->first();
                    $s['name'] = $climb->name;

                    $s['style'] = (isset($climb->style) ? $climb->style->title : '--');
                    $s['grade'] = (isset($climb->grade) ? $climb->grade->title : '--');
                    $s['exposure'] = (isset($climb->exposure) ? $climb->exposure->title : '--');

            return response()->json($s);
            }
    }

/* ----- ----- ----- ----- ----- ----- ----- -----
   ----- ----- ----- ----- ----- ----- ----- -----  */


    public function ajaxGradesForStyles(Request $request)
    {
/*        $styles_grades = StylesGrades::where('id_style', $request->id)->get();

            $s = array();

            if(!$styles_grades->isEmpty()) {
                foreach($styles_grades as $t)
                    $s[] = $t->grades;
            }   */
            $idStyle = $request->id;
            $ndx_grades = Grades::whereHas('stylesGrades', function($q) use($idStyle) {
                $q->where('id_style', $idStyle);
            })->get();
        return response()->json($ndx_grades);
    }


    public function ajaxUpdateDescription(Request $request)
    {
        $action = Climbs::findOrFail($request->id_climb);

            $action->description = $request->description;
        
        $action->save();

            if($action->wasChanged()) {
        
                return response()->json(['description' => $action->description]);
            }
    }


    public function ajaxUpdateWeatherObs(Request $request)
    {
        $action = Climbs::findOrFail($request->id_climb);

            $action->weather = $request->weather;
            $action->obs = $request->obs;
        
        $action->save();

            if($action->wasChanged()) {
        
                return response()->json(['weather' => $action->weather, 'obs' => $action->obs,]);
            }
    }


    public function ajaxStoreEquipment(Request $request)
    {
        $action = ClimbsEquipments::firstOrCreate(
                        $request->only('id_climb', 'id_equipment'),
                        $request->only('amount'));


        if($action->wasRecentlyCreated) {
            $equipments = $this->getClimbEquipments($request->id_climb);

        return response()->json($equipments);
        }
    }


    public function ajaxDestroyEquipment(Request $request)
    {
        $action = ClimbsEquipments::where('id_climb', $request->id_climb)
                                    ->where('id_equipment', $request->id_equipment)
                                    ->delete();

            if($action == true) {
                $equipments = $this->getClimbEquipments($request->id_climb);

            return response()->json($equipments); }
    }


    public function ajaxStoreFirstAscent(Request $request)
    {
        $action = FirstAscent::firstOrCreate(
                        $request->only('name'));

        if(!empty($action->id)) {

            $idFirstAscent = $action->id;
            unset($action);

                $action = ClimbsFirstAscent::firstOrCreate(
                            [   'id_climb' => $request->id_climb,
                                'id_firstascent' => $idFirstAscent]);
        }

        if($action->wasRecentlyCreated) {

            $firstascent = $this->getClimbFirstAscent($request->id_climb);
/*        $idClimb = $request->id_climb;

            $firstascent = FirstAscent::whereHas('climbsFirstAscent', function($q) use($idClimb) {
                $q->where('id_climb', $idClimb);
            })->get();  */

        return response()->json($firstascent); }
    }

    public function ajaxDestroyFirstAscent(Request $request)
    {
        $action = ClimbsFirstAscent::where('id_climb', $request->id_climb)
                                    ->where('id_firstascent', $request->id_firstascent)
                                    ->delete();

            if($action == true) {
                $firstascent = $this->getClimbFirstAscent($request->id_climb);

            return response()->json($firstascent); }
    }


/* ----- ----- ----- ----- ----- ----- ----- -----
   ----- ----- ----- ----- ----- ----- ----- -----  */


        private function getClimbEquipments($id_climb)
        {
            $climb_equipments = ClimbsEquipments::where('id_climb', $id_climb)->get();

                foreach($climb_equipments as $t)
                $equipments[] = [   'id'            => $t->equipments->id,
                                    'id_climb'      => $t->id_climb,
                                    'id_equipment'  => $t->id_equipment,
                                    'title'         => $t->equipments->title,
                                    'amount'        => $t->amount, ];

            return $equipments;
        }


        private function getClimbFirstAscent($id_climb)
        {
            $firstascent = [];

            $climb_firstascent = ClimbsFirstAscent::where('id_climb', $id_climb)->get();

                foreach($climb_firstascent as $t)
                $firstascent[] = [  'id'            => $t->firstascent->id,
                                    'id_climb'      => $t->id_climb,
                                    'id_firstascent'=> $t->id_firstascent,
                                    'name'         => $t->firstascent->name, ];

            return $firstascent;
        }


        public function autoCompleteFirstAscent(Request $request)
        {
            $data = FirstAscent::select("name")
                        ->where("name","LIKE","%{$request->input('query')}%")
                        ->limit('15')
                        ->get();

            return response()->json($data);
        }


}
