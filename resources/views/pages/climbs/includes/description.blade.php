<div class="col-xl-12">
        
        <div id="card_description" class="card">

            <div class="card-header py-3">
            <div class="row">
                <div class="col">
                    {{ __('Descrição') }}
                </div>

                <div class="col text-right">
                    <button id="btn_form_description" class="btn btn-sm btn-primary rounded-circle text-white p-1 px-2 ml-3"
                        data-toggle="tooltip" data-placement="right" title="{{ __('Editar') }}">
                        <i class="ni ni-ruler-pencil"></i>
                    </button>
                </div>
            </div>
            </div>

            <div id="body_description" class="card-body pt-3">
                {{ $climb->description }}
            </div>
        
        </div>


<!-- BEGIN: Form Descrição -->
    <div id="card_form_description" class="card d-none">
        <div class="card-header">
            <h3 class="mb-0">{{ __('Editar Descrição') }}</h3>
        </div>

            <div class="card-body">

                {{ Form::open(array('id' => 'form_edit_description', 'route' => 'climbs.description.update.ajax', 'method' => 'post')) }}

                {{ Form::hidden('id_climb', $climb->id, array('id' => 'id_climb', 'class' => 'form-control')) }}

                    <div class="form-row mb-3">
                        <div class="col">
                            {{ Form::textarea('description', $climb->description, array('id' => 'description', 'class' => 'form-control')) }}
                        </div>
                    </div>

                    {{ Form::submit(__('Salvar'), array('id' => 'btn_save_description', 'class' => 'btn btn-small btn-primary')) }}
                    {{ Form::button(__('Cancelar'), array('id' => 'btn_cancel_form_description', 'class' => 'btn btn-small btn-danger')) }}

                {{ Form::close() }}

            </div>
    </div>
<!-- END: Form Descrição -->

</div>


@push('js-include')
<script>
// --- BTN: Controles para data/form da descrição
    $('#btn_form_description, #btn_cancel_form_description').on('click',function(){
        $('#card_description, #card_form_description').toggleClass('d-none');
    });

// --- FORM: edita coluna de descrição da via
$( "#form_edit_description" ).submit(function( event ) {
    event.preventDefault();

    var id_climb = $(this['id_climb']).val();
    var description = $(this['description']).val();

        $.ajax({
            type        : 'POST',
            url         : '{{ route("climbs.description.update.ajax") }}',
            dataType    : "JSON",
                data    : {
                    "_token": "{{ csrf_token() }}",
                    id_climb: id_climb,
                    description: description,
                },
                    success: function(data){
                        var alert = '<strong>Info!</strong> Registro Ok!';

                        $('#body_description').html(data.description);
                        callAlerts(alert, 'primary');

                            $('#card_description, #card_form_description').toggleClass('d-none');
                    }
        });

    return false;
    });

</script>
@endpush