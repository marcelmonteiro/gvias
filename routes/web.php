<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});


Route::name('crags.')->group(function () {
    Route::get('/crags/create', 'CragsController@create')->name('create');
    Route::post('/crags/store', 'CragsController@store')->name('store');
    Route::get('/crags/{id}/climbs', 'CragsController@climbs')->name('climbs');
});


Route::name('climbs.')->group(function () {
    Route::get('/climbs/{id}', 'ClimbsController@show')->name('show');

    Route::get('/climbs/create/{id}', 'ClimbsController@create')->name('create');
    Route::post('/climbs/store', 'ClimbsController@store')->name('store');
    Route::post('/climbs/update', 'ClimbsController@update')->name('update');

        Route::get('/climbs/ajax/grades_styles', 'ClimbsController@ajaxGradesForStyles')->name('grades_styles.ajax');
        Route::get('/climbs/ajax/firstascent/autocomplete', 'ClimbsController@autoCompleteFirstAscent')->name('firstascent.autocomplete.ajax');

        Route::post('/climbs/ajax/update/description', 'ClimbsController@ajaxUpdateDescription')->name('description.update.ajax');
        Route::post('/climbs/ajax/update/weather_obs', 'ClimbsController@ajaxUpdateWeatherObs')->name('weather_obs.update.ajax');
        Route::post('/climbs/ajax/store/equipment', 'ClimbsController@ajaxStoreEquipment')->name('equipment.store.ajax');
        Route::post('/climbs/ajax/destroy/equipment', 'ClimbsController@ajaxDestroyEquipment')->name('equipment.destroy.ajax');
        Route::post('/climbs/ajax/store/firstascent', 'ClimbsController@ajaxStoreFirstAscent')->name('firstascent.store.ajax');
        Route::post('/climbs/ajax/destroy/firstascent', 'ClimbsController@ajaxDestroyFirstAscent')->name('firstascent.destroy.ajax');

});


Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});
