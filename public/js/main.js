
/* --- Função para chamada de alertas nas respostas do formulários Ajax
--- */
function callAlerts(txt, btnclass) {

    var alert = '<div class="alert alert-primary alert-dismissible fade show col-6" style="position:absolute;right:1rem;margin-top:-.50rem;z-index:999;" role="alert">' +
                    '<span class="alert-icon"><i class="ni ni-chat-round"></i></span>' +
                    '<span class="alert-text">' + txt + '</span>' +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '</div>';

        $(alert)
            .insertBefore('.content')
            .delay(3000)
            .fadeOut(function() {
                $(this).remove();
            });
}


/* --- Correção na logística do funcionamento do tooltip no template do Argon
   --- ** Define que tooltip deve desaparecer com o clique no elemento
--- */
    $('[data-toggle="tooltip"]').on('click', function () {
        $(this).tooltip('hide')
    })
