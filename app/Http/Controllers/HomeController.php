<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Crags;

use App\Styles;
use App\Exposure;

use App\StylesGrades;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $crags = Crags::orderby('name')->get();

        $styles = Styles::orderby('title')->get();
        $exposure = Exposure::orderby('title')->get();

        $styles_grades = StylesGrades::where('id_style', 1)->get();

        return view('home', compact('crags', 'styles', 'exposure', 'styles_grades'));
    }

}
