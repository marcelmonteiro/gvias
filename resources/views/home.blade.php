@extends('layouts.app')

@section('content')

<div class="container-fluid">

    <div class="header-body pt-3 pb-5 text-right">
        <a href="{{ route('crags.create') }}" class="btn btn-success">
            {{ __('Novo Setor') }}
        </a>
    </div>


<!-- BEGIN: Formulário Registrar Via -->
    <div id="row_add_climb" class="row mb-5 d-none">
    <div class="col-8 offset-2 bg-form p-4 rounded">

        {{ Form::open(array('id' => 'form_add_climb', 'route' => 'climbs.store', 'method' => 'post')) }}

            {{ Form::text('id_crag', null, array('class' => 'form-control')) }}

            <div class="form-row mb-3">
                {{ Form::label('name', __('Nome da Via')) }}
                {{ Form::text('name', null, array('class' => 'form-control')) }}
            </div>

            <div class="form-row mb-3">
                <div class="col">
                {{ Form::label('id_style', __('Estilo')) }}
                {{ Form::select('id_style', $styles->pluck('title', 'id'), null, array('class' => 'form-control', 'placeholder' => __('Selecione'))) }}
                </div>

                <div class="col">
                {{ Form::label('id_grade', __('Graduação')) }}
                {{ Form::select('id_grade', array(), null, array('class' => 'form-control')) }}
                </div>

                <div class="col">
                {{ Form::label('id_exposure', __('Exposição')) }}
                {{ Form::select('id_exposure', $exposure->pluck('title', 'id'), null, array('class' => 'form-control', 'placeholder' => __('Selecione'))) }}
                </div>
            </div>

            {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
            {{ Form::button('Cancelar', array('class' => 'btn btn-sm btn-danger btn_form_cancel')) }}

        {{ Form::close() }}
    </div>
    </div>
<!-- END: Formulário Registrar Via -->


    <div id="crags_data" class="row">

@foreach($crags as $crag)
            <div class="col-xl-4 col-lg-6">
                <div class="card card-stats mb-4">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted mb-0">
                                    <a href="{{ route('crags.climbs', $crag->id) }}">
                                    {{ $crag->name }}
                                    </a>
                                </h5>

                                <span class="small font-weight-bold mb-0">{{ $crag->climbs->count() }} vias registradas</span>
                            </div>

                            <div class="col-auto">
                                <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                    <i class="fas fa-users"></i>
                                </div>
                            </div>

                            <p class="mt-3 mb-0 pl-2">
                                <button 
                                    data-id="{{ $crag->id }}"
                                    class="btn btn-primary btn-sm btn_add_climb">
                                        {{ __('Adicionar Via') }}
                                </button>

                                <a href="#" class="btn btn-warning btn-sm">{{ __('Desativar Setor') }} </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
@endforeach

    </div>

    @include('layouts.footers.auth')

</div>

@endsection

@push('js')
<script>
    $('#crags_data').on('click','.btn_add_climb', function(){

        var idCrag = $(this).data('id');

            $('#row_add_climb').removeClass('d-none');

                resetFormAddClimb();

                $('#form_add_climb input[name="id_crag"]').val(idCrag);
    });

    $('#row_add_climb').on('click','.btn_form_cancel', function(){
        
        $('#row_add_climb').addClass('d-none');
        
            resetFormAddClimb();
    });


    $('#form_add_climb #id_style').on('change',function(){
        
        var idStyle = $(this).val();

            $.ajax({
                type        : 'GET',
                url         : '{{ route("climbs.grades_styles.ajax") }}',
                async       : false,
                dataType    : "JSON",
                    data    : {
                        id: idStyle,
                    },
                        success: function(data){
                            var html = '';
                            var i;

                            for(i=0; i<data.length; i++)
                            {
                                html += '<option value="' + data[i].id +'">' + data[i].title + '</option>';
                            }
                            
                                $('#form_add_climb #id_grade').html(html);
                        }
            });


            if(idStyle == 1) {
                $("#form_add_climb #id_exposure").attr('disabled',true);
            }

            else {
                $("#form_add_climb #id_exposure").attr('disabled',false);
            }
    });


        function resetFormAddClimb() {
            $('#form_add_climb input[name="id_crag"]').val('');
            $('#form_add_climb input[name="name"]').val('');
            $('#form_add_climb select[name="id_style"]').val('');
            $('#form_add_climb select[name="id_grade"]').val('');
            $('#form_add_climb select[name="id_exposure"]').val('');
                $('#form_add_climb select[name="id_grade"]').html('');
                $('#form_add_climb select[name="id_exposure"]').attr('disabled',false);
        }

</script>
@endpush
