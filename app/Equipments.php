<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipments extends Model
{
    protected $table = 'g_ndx_equipments';

    protected $fillable = [];

    public $timestamps = false;

}