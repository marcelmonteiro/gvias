        <div class="col-xl-4">

<!-- BEGIN: Card Equipamentos -->
        <div id="card_equipments" class="card">

            <div class="card-header border-0">
                <div class="row align-items-center">
                <div class="col">
                    <h3 class="mb-0">{{ __('Equipamentos') }}</h3>
                </div>

                <div class="col text-right">
                    <button id="btn_form_equipment" class="btn btn-sm btn-info rounded-circle text-white p-1 px-2 ml-3"
                        data-toggle="tooltip" data-placement="right" title="{{ __('Adicionar') }}">
                        <i class="ni ni-fat-add"></i>
                    </button>
                </div>
                </div>
            </div>

            <div class="table-responsive">
                <table id="table_equipments" class="table align-items-center table-flush">

                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Qut.</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    </tbody>

                </table>
            </div>

        </div>
<!-- END: Card Equipamentos -->


<!-- BEGIN: Form Equipamentos -->
            <div id="card_form_equipment" class="card d-none">
                <div class="card-header">
                    <h3 class="mb-0">{{ __('Adicionar Equipamento') }}</h3>
                </div>

                <div class="card-body">

                    {{ Form::open(array('id' => 'form_add_equipment', 'route' => 'climbs.equipment.store.ajax', 'method' => 'post')) }}

                    {{ Form::hidden('id_climb', $climb->id, array('id' => 'id_climb', 'class' => 'form-control')) }}

                    <div class="form-row mb-3">
                        <div class="col">
                            {{ Form::label('id_equipment', __('Equipamento'), array('class' => 'small font-weight-bold')) }}
                            {{ Form::select('id_equipment', $ndx_equipments->pluck('title', 'id'), null, array('id' => 'id_equipment', 'class' => 'form-control', 'placeholder' => 'Selecione')) }}
                        </div>

                        <div class="col">
                            {{ Form::label('amount', __('Quantidade'), array('class' => 'small font-weight-bold')) }}
                            {{ Form::text('amount', null, array('id' => 'amount', 'class' => 'form-control', 'placeholder' => 'Quantidade')) }}
                        </div>
                    </div>

                        {{ Form::submit(__('Salvar'), array('id' => 'btn_save_equipment', 'class' => 'btn btn-small btn-primary')) }}
                        {{ Form::button(__('Cancelar'), array('id' => 'btn_cancel_form_equipment', 'class' => 'btn btn-small btn-danger')) }}

                        {{ Form::close() }}

                </div>
            </div>
<!-- END: Form Equipamentos -->

        </div>


@push('js-include')
<script>
/* ----- ----- ----- ----- ----- ----- ----- -----
   --- JS: Equipments
   ----- ----- ----- ----- ----- ----- ----- -----  */

// --- BTN: Controles para Data/Form dos EQUIPAMENTOS
    $('#btn_form_equipment, #btn_cancel_form_equipment').on('click',function(){
        resetFormEquipments();
        $('#card_equipments, #card_form_equipment').toggleClass('d-none');
    });


// --- FORM: criar registro de equipamento vinculado
    $( "#form_add_equipment" ).submit(function( event ) {
    event.preventDefault();

    var id_climb = $('#id_climb').val();
    var id_equipment = $('#id_equipment').val();
    var amount = $('#amount').val();

        $.ajax({
            type        : 'POST',
            url         : '{{ route("climbs.equipment.store.ajax") }}',
            dataType    : "JSON",
                data    : {
                    "_token": "{{ csrf_token() }}",
                    id_climb: id_climb,
                    id_equipment: id_equipment,
                    amount: amount,
                },
                    success: function(data){
                        var alert = '<strong>Info!</strong> Registro Ok!';

                        showEquipments(data);
                        callAlerts(alert, 'primary');

                            resetFormEquipments();
                            $('#card_equipments, #card_form_equipment').toggleClass('d-none');
                    }
        });

    return false;
    });


// --- BTN-ACTION: excluir registro de equipamento vinculado
    $('#table_equipments tbody').on('click','.btn-danger',function(){

    var id = $(this).data('id').split('_');

        $.ajax({
            type        : 'POST',
            url         : '{{ route("climbs.equipment.destroy.ajax") }}',
            dataType    : "JSON",
            data    : {
                    "_token": "{{ csrf_token() }}",
                    id_climb: id[0],
                    id_equipment: id[1],
                },
                    success: function(data){
                        var alert = '<strong>Info:</strong> registro removido!';

                        showEquipments(data);
                        callAlerts(alert, 'primary');

                            resetFormEquipments();
                    }
        });

    return false;
    });

// --- Reset dos campos do formulário de equipamentos
        function resetFormEquipments() {
            $('#form_add_equipment select[name="id_equipment"]').val("");
            $('#form_add_equipment input[name="amount"]').val("");
        }

// --- Alimenta layout de equipamentos
        function showEquipments(data) {

        var html = '';
        var i;

            for(i=0; i<data.length; i++)
            {
            html += '<tr>' +
                        '<th scope="row">' + data[i].amount + '</th>' +
                        '<td>' + data[i].title + '</td>' +
                        '<td>' +
                        '<button class="btn btn-sm btn-danger align-top rounded-circle text-white p-1 px-2 ml-3" ' +
                            'data-id="' + data[i].id_climb + '_' + data[i].id_equipment + '"' +
                            'data-toggle="tooltip" data-placement="right" title="{{ __('Remover') }}">' +
                            '<i class="ni ni-fat-remove"></i>' +
                        '</button>' +
                        '</td>' +
                    '</tr>';
            }

            $('#table_equipments tbody').html(html);
        }
</script>
@endpush