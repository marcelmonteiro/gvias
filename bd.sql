-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.11-MariaDB - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table gvias.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gvias.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table gvias.g_climbs
CREATE TABLE IF NOT EXISTS `g_climbs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_crag` int(11) DEFAULT NULL,
  `id_style` int(11) DEFAULT NULL,
  `id_grade` int(11) DEFAULT NULL,
  `id_exposure` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `weather` text DEFAULT NULL,
  `obs` text DEFAULT NULL,
  `extension` int(11) DEFAULT NULL,
  `pitches` int(11) DEFAULT NULL,
  `firstascent` date DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table gvias.g_climbs: ~7 rows (approximately)
/*!40000 ALTER TABLE `g_climbs` DISABLE KEYS */;
INSERT INTO `g_climbs` (`id`, `id_crag`, `id_style`, `id_grade`, `id_exposure`, `name`, `description`, `weather`, `obs`, `extension`, `pitches`, `firstascent`, `id_user`, `created_at`, `updated_at`) VALUES
	(1, 1, NULL, NULL, NULL, 'Primeiro as Damas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 2, NULL, NULL, NULL, 'Sargento Tainha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 2, NULL, NULL, NULL, 'Recruta Zero', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 1, NULL, NULL, NULL, 'Mula Sem Cabeça', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 1, 1, 5, NULL, 'Mula 41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 1, 2, NULL, 1, 'Mula 4', 'Descrição da via 25', 'Clima Teste 12', 'Obs Teste 13', NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 2, 1, 7, NULL, 'Mula 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 1, 4, NULL, 2, 'Mula 6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `g_climbs` ENABLE KEYS */;

-- Dumping structure for table gvias.g_crags
CREATE TABLE IF NOT EXISTS `g_crags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `approach` text DEFAULT NULL,
  `lat` float(10,6) DEFAULT NULL,
  `lon` float(10,6) DEFAULT NULL,
  `obs` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table gvias.g_crags: ~2 rows (approximately)
/*!40000 ALTER TABLE `g_crags` DISABLE KEYS */;
INSERT INTO `g_crags` (`id`, `name`, `approach`, `lat`, `lon`, `obs`) VALUES
	(1, 'Morro do Boi', 'O acesso é pela BR101. Saindo de Balneário em direção Florianópolis, antes de começar a subir o Morro do Boi, na altura do Posto Tigrão (saída 138) , pegar a Marginal Oeste e seguir até o final. Você chegará de frente com o elevado da BR101, logo ao lado deste elevado está a porteira de entrada do sítio que abriga o setor. Observando o morro com cuidado já se avista a rocha atrás de uma torre elétrica. Entre na propriedade e siga as placas de orientação para as vias. Mantenha as porteiras fechadas.', -27.035723, -48.606892, 'Base das vias bastante sombreada, porém parede com sol quase o dia inteiro.'),
	(2, 'Pedreira de Itapema', 'Localizado no bairro Ilhota, município de Itapema, é possível avista-lo desde a BR 101, perto da Polícia Rodoviária. Procure a entrada da trilha ao lado da casa verde: rua 1214, n.163, bairro Ilhota.', -27.063303, -48.602421, 'Todas vias possíveis de rapelar com uma corda de 60m. Todas as reuniões estão preparadas com duas chapeletas. Pense na possibilidade de levar mais uma costura para costurar a parada, alguns finais são bem técnicos, difíceis de puxar a solteira. Descrição dos “Equipos” não inclui equipamento para reunião, leve para todas as vias. Use capacete.');
/*!40000 ALTER TABLE `g_crags` ENABLE KEYS */;

-- Dumping structure for table gvias.g_ndx_equipments
CREATE TABLE IF NOT EXISTS `g_ndx_equipments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table gvias.g_ndx_equipments: ~2 rows (approximately)
/*!40000 ALTER TABLE `g_ndx_equipments` DISABLE KEYS */;
INSERT INTO `g_ndx_equipments` (`id`, `title`) VALUES
	(1, 'Costura'),
	(2, 'Costura Longa'),
	(3, 'Camelot');
/*!40000 ALTER TABLE `g_ndx_equipments` ENABLE KEYS */;

-- Dumping structure for table gvias.g_ndx_exposure
CREATE TABLE IF NOT EXISTS `g_ndx_exposure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table gvias.g_ndx_exposure: ~2 rows (approximately)
/*!40000 ALTER TABLE `g_ndx_exposure` DISABLE KEYS */;
INSERT INTO `g_ndx_exposure` (`id`, `title`) VALUES
	(1, 'E1'),
	(2, 'E2'),
	(3, 'E3');
/*!40000 ALTER TABLE `g_ndx_exposure` ENABLE KEYS */;

-- Dumping structure for table gvias.g_ndx_firstascent
CREATE TABLE IF NOT EXISTS `g_ndx_firstascent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table gvias.g_ndx_firstascent: ~6 rows (approximately)
/*!40000 ALTER TABLE `g_ndx_firstascent` DISABLE KEYS */;
INSERT INTO `g_ndx_firstascent` (`id`, `name`) VALUES
	(1, 'Marcel Monteiro'),
	(2, 'Rian bywall'),
	(3, 'Neri bywall'),
	(4, 'Pedro'),
	(5, 'Luiz Naves'),
	(6, 'Luizinho');
/*!40000 ALTER TABLE `g_ndx_firstascent` ENABLE KEYS */;

-- Dumping structure for table gvias.g_ndx_grades
CREATE TABLE IF NOT EXISTS `g_ndx_grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table gvias.g_ndx_grades: ~8 rows (approximately)
/*!40000 ALTER TABLE `g_ndx_grades` DISABLE KEYS */;
INSERT INTO `g_ndx_grades` (`id`, `title`) VALUES
	(1, 'V0'),
	(2, 'V0+'),
	(3, 'V1'),
	(4, 'V1+'),
	(5, 'V2-'),
	(6, 'V2'),
	(7, 'V2+'),
	(8, 'V3-'),
	(9, 'V3'),
	(10, 'V3+'),
	(11, 'VII'),
	(12, 'VIII');
/*!40000 ALTER TABLE `g_ndx_grades` ENABLE KEYS */;

-- Dumping structure for table gvias.g_ndx_styles
CREATE TABLE IF NOT EXISTS `g_ndx_styles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table gvias.g_ndx_styles: ~4 rows (approximately)
/*!40000 ALTER TABLE `g_ndx_styles` DISABLE KEYS */;
INSERT INTO `g_ndx_styles` (`id`, `title`) VALUES
	(1, 'Boulder'),
	(2, 'Esportiva'),
	(3, 'Tradicional'),
	(4, 'Mista');
/*!40000 ALTER TABLE `g_ndx_styles` ENABLE KEYS */;

-- Dumping structure for table gvias.g_rlt_climbs_equipments
CREATE TABLE IF NOT EXISTS `g_rlt_climbs_equipments` (
  `id_climb` int(11) DEFAULT NULL,
  `id_equipment` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table gvias.g_rlt_climbs_equipments: ~6 rows (approximately)
/*!40000 ALTER TABLE `g_rlt_climbs_equipments` DISABLE KEYS */;
INSERT INTO `g_rlt_climbs_equipments` (`id_climb`, `id_equipment`, `amount`) VALUES
	(3, 3, 5),
	(6, 3, 5),
	(47, 3, 25),
	(NULL, 3, NULL),
	(5, 1, 31),
	(5, 3, 16);
/*!40000 ALTER TABLE `g_rlt_climbs_equipments` ENABLE KEYS */;

-- Dumping structure for table gvias.g_rlt_climbs_firstascent
CREATE TABLE IF NOT EXISTS `g_rlt_climbs_firstascent` (
  `id_climb` int(11) NOT NULL,
  `id_firstascent` int(11) NOT NULL,
  UNIQUE KEY `id_climb_id_firstascent` (`id_climb`,`id_firstascent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table gvias.g_rlt_climbs_firstascent: ~2 rows (approximately)
/*!40000 ALTER TABLE `g_rlt_climbs_firstascent` DISABLE KEYS */;
INSERT INTO `g_rlt_climbs_firstascent` (`id_climb`, `id_firstascent`) VALUES
	(5, 3),
	(5, 5),
	(6, 1);
/*!40000 ALTER TABLE `g_rlt_climbs_firstascent` ENABLE KEYS */;

-- Dumping structure for table gvias.g_rlt_styles_grades
CREATE TABLE IF NOT EXISTS `g_rlt_styles_grades` (
  `id_style` int(11) NOT NULL,
  `id_grade` int(11) NOT NULL,
  KEY `FK_Grade_Style` (`id_grade`),
  KEY `FK_Style_Grade` (`id_style`),
  CONSTRAINT `FK_Grade_Style` FOREIGN KEY (`id_grade`) REFERENCES `g_ndx_grades` (`id`),
  CONSTRAINT `FK_Style_Grade` FOREIGN KEY (`id_style`) REFERENCES `g_ndx_styles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table gvias.g_rlt_styles_grades: ~12 rows (approximately)
/*!40000 ALTER TABLE `g_rlt_styles_grades` DISABLE KEYS */;
INSERT INTO `g_rlt_styles_grades` (`id_style`, `id_grade`) VALUES
	(1, 1),
	(1, 2),
	(1, 3),
	(1, 4),
	(1, 5),
	(1, 6),
	(1, 7),
	(1, 8),
	(1, 9),
	(1, 10),
	(2, 11),
	(2, 12);
/*!40000 ALTER TABLE `g_rlt_styles_grades` ENABLE KEYS */;

-- Dumping structure for table gvias.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gvias.migrations: ~9 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2020_03_30_124223_create_permission_tables', 1),
	(5, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
	(6, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
	(7, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
	(8, '2016_06_01_000004_create_oauth_clients_table', 2),
	(9, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table gvias.model_has_permissions
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gvias.model_has_permissions: ~0 rows (approximately)
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;

-- Dumping structure for table gvias.model_has_roles
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gvias.model_has_roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;

-- Dumping structure for table gvias.oauth_access_tokens
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gvias.oauth_access_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Dumping structure for table gvias.oauth_auth_codes
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gvias.oauth_auth_codes: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Dumping structure for table gvias.oauth_clients
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gvias.oauth_clients: ~2 rows (approximately)
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'Laravel Personal Access Client', 'glGop9LDdloqSBv42PeEBIGglfsdcvUBFb3ymKFq', 'http://localhost', 1, 0, 0, '2020-03-30 12:47:19', '2020-03-30 12:47:19'),
	(2, NULL, 'Laravel Password Grant Client', '7VQvaO1v64pwGArIP5il9sKLi88NPTMQPPSaYtoP', 'http://localhost', 0, 1, 0, '2020-03-30 12:47:19', '2020-03-30 12:47:19');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Dumping structure for table gvias.oauth_personal_access_clients
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gvias.oauth_personal_access_clients: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
	(1, 1, '2020-03-30 12:47:19', '2020-03-30 12:47:19');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Dumping structure for table gvias.oauth_refresh_tokens
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gvias.oauth_refresh_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- Dumping structure for table gvias.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gvias.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table gvias.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gvias.permissions: ~0 rows (approximately)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table gvias.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gvias.roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table gvias.role_has_permissions
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gvias.role_has_permissions: ~0 rows (approximately)
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;

-- Dumping structure for table gvias.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gvias.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'admin@admin.com', '2020-03-30 13:56:28', '$2y$10$ZuXCpzFuxQCPfdr99vSCaemAIjnbgC/jjgQoIgOMTke0QRKWAA5hG', 'kq9zpkJf7NVinp0T2HCOXVY989wShcIsd5zld5hYXvOZewdwCMdN0NfOzv9u', '2020-03-30 13:56:28', '2020-03-30 13:56:28');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
