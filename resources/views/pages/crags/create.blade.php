@extends('layouts.app', [
            'title'         => 'Crags',
        ])


@section('content')
    <div class="content">
    <div class="container-fluid">

        <div class="row mb-3">
            <h1>{{ __('Registrar Novo Setor') }}</h1>
        </div>


        <div class="row justify-content-md-center">

<!-- FORM -->
            <div class="col-8 bg-form p-4 rounded">

                {{ Form::open(array('route' => 'crags.store', 'method' => 'post')) }}

                <div class="form-row mb-3">
                    {{ Form::label('name', 'Nome') }}
                    {{ Form::text('name', null, array('class' => 'form-control')) }}<br>
                </div>

                <div class="form-row mb-3">
                    {{ Form::label('url_maps', 'Localização (URL Google Maps)') }}
                    {{ Form::text('url_maps', null, array('class' => 'form-control')) }}<br>
                </div>

                <div class="form-row mb-3">
                    {{ Form::label('approach', 'Acesso') }}
                    {{ Form::textarea('approach', null, array('id' => 'approach', 'class' => 'form-control')) }}<br>
                </div>

                <div class="form-row mb-3">
                    {{ Form::label('obs', 'Observações') }}
                    {{ Form::textarea('obs', null, array('id' => 'obs', 'class' => 'form-control')) }}<br>
                </div>

                    {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}
            </div>
<!-- END: FORM -->

        </div>

    </div>
    </div>
@endsection

@push('js')
{{--
    <script>
    ClassicEditor
        .create( document.querySelector( '#descricao' ) )
            .then( editor => {
                console.log( editor );
            })
            .catch( error => {
                console.error( error );
            });
    </script>
--}}
@endpush
