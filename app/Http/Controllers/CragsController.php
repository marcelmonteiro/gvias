<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Crags;
use App\Climbs;


class CragsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function create()
    {
        return view ('pages.crags.create');
    }


    public function store(Request $request)
    {
            $location = null;
            preg_match('/@(\-?[0-9]+\.[0-9]+),(\-?[0-9]+\.[0-9]+)/', $request->url_maps, $location);

        $action = Crags::firstOrCreate(
            $request->only('name'),
            [
                'approach'  => $request->approach,
                'lat'       => @$location[1],
                'lon'       => @$location[2],
                'obs'       => $request->obs,
            ]);

        return redirect()->route('home');
    }


    public function climbs($id)
    {
        $climbs = $this->getClimbs($id);

        return view ('pages.crags.climbs', compact('climbs'));
    }


    private function getClimbs($id)
    {

        $data = Climbs::where('id_crag', $id)
                        ->orderby('name')
                        ->get();

        return $data;
    }
/*  $action = new Crags;
    $action->title = $request->title;
    $action->create();  */

}
