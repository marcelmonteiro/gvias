<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crags extends Model
{
    protected $table = 'g_crags';

    protected $fillable = [
        'name', 'approach', 'lat', 'lon', 'obs'
    ];
//    protected $guarded = ['lat'];

    public $timestamps = false;


    public function climbs() {
        return $this->hasMany('App\Climbs', 'id_crag', 'id');	}

}
