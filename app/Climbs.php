<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Climbs extends Model
{
    protected $table = 'g_climbs';

    protected $fillable = [
        'id_crag',
        'name',
        'id_style', 'id_grade', 'id_exposure'
    ];
//    protected $guarded = ['lat'];

    public $timestamps = false;


        public function style()
        {
            return $this->belongsTo('App\Styles', 'id_style', 'id');
        }

        public function grade()
        {
            return $this->belongsTo('App\Grades', 'id_grade', 'id');
        }

        public function exposure()
        {
            return $this->belongsTo('App\Exposure', 'id_exposure', 'id');
        }


        public function equipments()
        {
            return $this->belongsToMany(
                            'App\Equipments',
                            'App\ClimbsEquipments',
                            'id_climb',
                            'id_equipment')
                        ->withPivot('amount');
        }
#       return $this->hasManyThrough('App\Equipments', 'App\ClimbsEquipments', 'id_climb', 'id', 'id', 'id_equipment'); }

        public function firstascent()
        {
            return $this->belongsToMany('App\FirstAscent', 'App\ClimbsFirstAscent', 'id_climb', 'id_firstascent');
        }

}
