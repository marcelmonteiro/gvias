<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Styles extends Model
{
    protected $table = 'g_ndx_styles';

    protected $fillable = [];

    public $timestamps = false;
}
