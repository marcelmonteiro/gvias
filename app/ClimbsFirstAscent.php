<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClimbsFirstAscent extends Model
{
    protected $table = 'g_rlt_climbs_firstascent';

    protected $fillable = ['id_climb', 'id_firstascent'];

    public $timestamps = false;


        public function firstascent()
        {
            return $this->belongsTo('App\FirstAscent', 'id_firstascent', 'id');
        }

        public function climbs()
        {
            return $this->belongsTo('App\Climbs', 'id_climb', 'id');
        }
}
