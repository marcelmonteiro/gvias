@extends('layouts.app', [
            'title'         => 'Crags',
        ])


@section('content')
    <div class="content">
    <div class="container-fluid">


    <div class="row mb-4 d-flex justify-content-end">
    <div class="col-6">

<ul class="nav nav-pills nav-fill flex-column flex-sm-row" id="tabs-text" role="tablist">
    <li class="nav-item">
        <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-text-1-tab" data-toggle="tab" href="#tabs-text-1" role="tab" aria-controls="tabs-text-1" aria-selected="true">
            {{ __('Informações') }}</a>
    </li>

    <li class="nav-item">
        <a class="nav-link mb-sm-3 mb-md-0" id="tabs-text-2-tab" data-toggle="tab" href="#tabs-text-2" role="tab" aria-controls="tabs-text-2" aria-selected="false">
            {{ __('Setores') }}</a>
    </li>
</ul>
</div>
</div>



<div class="table-responsive">

    <table class="table table-striped align-items-center">

        <thead class="thead-light">
        <tr>
            <th scope="col" class="sort" data-sort="name">&nbsp;</th>
            <th scope="col" class="sort" data-sort="name">Nome</th>
            <th scope="col" class="sort" data-sort="style">Estilo</th>
            <th scope="col" class="sort" data-sort="buttons">&nbsp;</th>
        </tr>
        </thead>

        <tbody class="list">

@foreach($climbs as $climb)
        <tr>
                <th scope="row">{{ $climb->id }}</th>
                <td scope="row">
                    <a href="{{ route('climbs.show', $climb->id) }}">
                        {{ $climb->name }}
                    </a>
                </td>
                <td scope="row">Estilo</td>
                <td scope="row">--</td>
        </tr>
@endforeach

        </tbody>

    </table>

</div>

    </div>
    </div>
@endsection


@push('js')
{{--
    <script>
    ClassicEditor
        .create( document.querySelector( '#descricao' ) )
            .then( editor => {
                console.log( editor );
            })
            .catch( error => {
                console.error( error );
            });
    </script>
--}}
@endpush
