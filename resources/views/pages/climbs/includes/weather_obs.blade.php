<div class="col-xl-8">
        
        <div id="card_weather_obs" class="card">

            <div class="card-header py-3">
            <div class="row">
                <div class="col">
                    {{ __('Clima & Observações') }}
                </div>

                <div class="col text-right">
                    <button id="btn_form_weather_obs" class="btn btn-sm btn-primary rounded-circle text-white p-1 px-2 ml-3"
                        data-toggle="tooltip" data-placement="right" title="{{ __('Editar') }}">
                        <i class="ni ni-ruler-pencil"></i>
                    </button>
                </div>
            </div>
            </div>

            <div class="card-body pt-3">
                <span id="body_weather">
                    {{ $climb->weather }}
                </span>
                    
                    <hr />

                <span id="body_obs">
                    {{ $climb->obs }}
                </span>
            </div>
        
        </div>


<!-- BEGIN: Form Clima / Observações -->
<div id="card_form_weather_obs" class="card d-none">
        <div class="card-header">
            <h3 class="mb-0">{{ __('Editar Clima / Observações') }}</h3>
        </div>

            <div class="card-body">

                {{ Form::open(array('id' => 'form_edit_weather_obs', 'route' => 'climbs.weather_obs.update.ajax', 'method' => 'post')) }}

                {{ Form::hidden('id_climb', $climb->id, array('id' => 'id_climb', 'class' => 'form-control')) }}

                    <div class="form-row mb-3">
                        <div class="col">
                            {{ Form::label('weather', __('Clima'), array('class' => 'small font-weight-bold')) }}
                            {{ Form::textarea('weather', $climb->weather, array('id' => 'weather', 'class' => 'form-control')) }}
                        </div>

                        <div class="col">
                            {{ Form::label('obs', __('Observações'), array('class' => 'small font-weight-bold')) }}
                            {{ Form::textarea('obs', $climb->weather, array('id' => 'obs', 'class' => 'form-control')) }}
                        </div>
                    </div>

                    {{ Form::submit(__('Salvar'), array('id' => 'btn_save_weather_obs', 'class' => 'btn btn-small btn-primary')) }}
                    {{ Form::button(__('Cancelar'), array('id' => 'btn_cancel_form_weather_obs', 'class' => 'btn btn-small btn-danger')) }}

                {{ Form::close() }}

            </div>
    </div>
<!-- END: Form Clima / Observações -->

</div>

@push('js-include')
<script>
// --- BTN: Controles para data/form da clima / observações
    $('#btn_form_weather_obs, #btn_cancel_form_weather_obs').on('click',function(){
        $('#card_weather_obs, #card_form_weather_obs').toggleClass('d-none');
    });

// --- FORM: edita as colunas de clima e observações da via
$( "#form_edit_weather_obs" ).submit(function( event ) {
    event.preventDefault();

    var id_climb = $(this['id_climb']).val();
    var weather = $(this['weather']).val();
    var obs = $(this['obs']).val();

        $.ajax({
            type        : 'POST',
            url         : '{{ route("climbs.weather_obs.update.ajax") }}',
            dataType    : "JSON",
                data    : {
                    "_token": "{{ csrf_token() }}",
                    id_climb: id_climb,
                    weather: weather,
                    obs: obs,
                },
                    success: function(data){
                        var alert = '<strong>Info!</strong> Registro Ok!';

                        $('#body_weather').html(data.weather);
                        $('#body_obs').html(data.obs);
                        callAlerts(alert, 'primary');

                            $('#card_weather_obs, #card_form_weather_obs').toggleClass('d-none');
                    }
        });

    return false;
    });
</script>
@endpush