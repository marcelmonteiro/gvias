<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exposure extends Model
{
    protected $table = 'g_ndx_exposure';

    protected $fillable = [];

    public $timestamps = false;
}
