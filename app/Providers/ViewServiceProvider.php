<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{

    public function boot()
    {
        View::composer(
            ['layouts.navbars.sidebar'], 'App\Http\View\Composers\MenuComposer'
        );
    }

}
