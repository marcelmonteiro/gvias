<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grades extends Model
{
    protected $table = 'g_ndx_grades';

    protected $fillable = [];

    public $timestamps = false;


        public function stylesGrades() {
            return $this->belongsTo('App\StylesGrades', 'id', 'id_grade');	}

}
