<!-- BEGIN: Header Info -->
    <div id="row_info" class="row mb-4">
    <div class="col-12">

        <!-- Title-Blocos -->
        <div class="card bg-primary mr-4 d-inline-block">
        <div id="row_info_name" class="card-body px-4 pt-1 pb-2 h1 m-0 text-white">
            {{ $climb->name }}
        </div>
        </div>

        <div class="card bg-default mr-4 d-inline-block">
        <div class="card-body px-4 pt-1 pb-2 h1 m-0 text-white">
            <span id="row_info_style">
                {{ isset($climb->style) ? $climb->style->title : '--' }}
            </span>
        </div>
        </div>

        <div class="card bg-warning d-inline-block">
        <div class="card-body px-4 pt-1 pb-2 h1 m-0 text-white">
            <span id="row_info_grade">
                {{ isset($climb->grade) ? $climb->grade->title : '--' }}
            </span>

                    /

            <span id="row_info_exposure">
                {{ isset($climb->exposure) ? $climb->exposure->title : '--' }}
            </span>
        </div>
        </div>

        <!-- Button Add -->
        <button id="btn_form_info" class="btn btn-primary align-top rounded-circle text-white p-1 px-2 ml-3"
            data-toggle="tooltip" data-placement="right" title="{{ __('Editar') }}">
            <i class="ni ni-ruler-pencil"></i>
        </button>

    </div>
    </div>
<!--END: Header Info -->


<!-- BEGIN: Formulário Editar Info da Via -->
    <div id="row_edit_info" class="row mb-5 d-none">
    <div class="col-12">

        {{ Form::open(array('id' => 'form_edit_climb', 'route' => 'climbs.update', 'method' => 'post', 'class' => 'bg-form rounded p-3')) }}

            {{ Form::hidden('id_climb', $climb->id, array('class' => 'form-control')) }}

            <div class="form-row mb-3">
                {{ Form::text('name', $climb->name, array('class' => 'form-control')) }}
            </div>

            <div class="form-row mb-3">
                <div class="col">
                {{ Form::select('id_style', $ndx_styles->pluck('title', 'id'), $climb->id_style, array('id' => 'id_style', 'class' => 'form-control', 'placeholder' => __('Selecione'))) }}
                </div>

                <div class="col">
                {{ Form::select('id_grade', $ndx_grades->pluck('title', 'id'), $climb->id_grade, array('id' => 'id_grade', 'class' => 'form-control', 'placeholder' => __('Selecione'))) }}
                </div>

                <div class="col">
                {{ Form::select('id_exposure', $ndx_exposure->pluck('title', 'id'), $climb->id_exposure, array('id' => 'id_exposure', 'class' => 'form-control', 'placeholder' => __('Selecione'))) }}
                </div>
            </div>

            {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
            {{ Form::button('Cancelar', array('id' => 'btn_cancel_form_info', 'class' => 'btn btn-sm btn-danger btn_form_cancel')) }}

        {{ Form::close() }}
    </div>
    </div>
<!-- END: Formulário Registrar Via -->

@push('js-include')
<script>
/* ----- ----- ----- ----- ----- ----- ----- -----
   --- JS: Info (header)
   ----- ----- ----- ----- ----- ----- ----- -----  */

// --- BTN: Controles para Data/Form das INFORMAÇÕES (no header do container)
$('#btn_form_info, #btn_cancel_form_info').on('click',function(){
        $('#row_edit_info').toggleClass('d-none');
    });

// --- FORM: atualiza informações da via
    $("#form_edit_climb").submit(function( event ) {
    event.preventDefault();

    var id_climb = $(this['id_climb']).val();
    var name = $(this['name']).val();
    var id_style = $(this['id_style']).val();
    var id_grade = $(this['id_grade']).val();
    var id_exposure = $(this['id_exposure']).val();

        $.ajax({
            type        : 'POST',
            url         : '{{ route("climbs.update") }}',
            dataType    : "JSON",
                data    : {
                    "_token": "{{ csrf_token() }}",
                    id:             id_climb,
                    name:           name,
                    id_style:       id_style,
                    id_grade:       id_grade,
                    id_exposure:    id_exposure,
                },
                    success: function(data){
                        var alert = '<strong>Info!</strong> Registro Ok!';

                        $('#row_info_name').html(data.name);
                        $('#row_info_style').html(data.style);
                        $('#row_info_grade').html(data.grade);
                        $('#row_info_exposure').html(data.exposure);

                            $('#row_edit_info').addClass('d-none');
                    }
        });

    return false;
    });

// --- FORM-CONTROL: Alimenta select de Graduação (id_grade) a partir da posição do select de Estilo (id_style)
    $('#form_edit_climb #id_style').on('change',function(){

    var idStyle = $(this).val();

        $.ajax({
            type        : 'GET',
            url         : '{{ route("climbs.grades_styles.ajax") }}',
            async       : false,
            dataType    : "JSON",
                data    : {
                    id: idStyle,
                },
                    success: function(data){
                        var html = '';
                        var i;

                        for(i=0; i<data.length; i++)
                        {
                            html += '<option value="' + data[i].id +'">' + data[i].title + '</option>';
                        }

                            html += '<option value="">{{ __('sem graduação') }}</option>';

                        $('#form_edit_climb #id_grade').html(html);
                    }
        });

            if(idStyle == 1) {
                $("#form_edit_climb #id_exposure").attr('disabled',true).val('');
            }

            else {
                $("#form_edit_climb #id_exposure").attr('disabled',false);
            }
    });
</script>
@endpush
