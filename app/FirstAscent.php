<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FirstAscent extends Model
{
    protected $table = 'g_ndx_firstascent';

    protected $fillable = ['name'];

    public $timestamps = false;


    public function climbsFirstAscent() {
        return $this->belongsTo('App\ClimbsFirstAscent', 'id', 'id_firstascent');	}

}
