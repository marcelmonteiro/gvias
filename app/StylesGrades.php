<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StylesGrades extends Model
{
    protected $table = 'g_rlt_styles_grades';

    protected $fillable = [];

    public $timestamps = false;


        public function styles() {
            return $this->belongsTo('App\Styles', 'id_style', 'id');	}


        public function grades() {
            return $this->belongsTo('App\Grades', 'id_grade', 'id');	}
}